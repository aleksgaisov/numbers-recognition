# Handwritten numbers recognition

![Neural network in work](./pix/header.jpg)

This program creates a simple neural network, whose goal is to identify handwritten numbers from 0 to 9.
Thus, it accepts a PNG image 9x9 pixels large, with white background and black number on it.
As a result, it provides a digit that it "sees" on the picture.

## Usage

The python code is written within the [Jupyter Notebook](https://jupyter.org/) and, therefore, should be executed accordingly.
[Install](https://jupyter.org/install) Jupyter Notebook on your system and open the `numbers-recognition.ipynb` file.

A `template.xcf` [GIMP](https://www.gimp.org/) file is provided as a template for creating appropriate images.

## Explanations

I was heavily relying on [this article](http://galaxy.agh.edu.pl/~vlsi/AI/backp_t_en/backprop.html) while working on the project.
It roughly describes the algorithm of teaching a simple neural network through **backpropagation**.

The program features 3 classes (Neuron, Network, Sample), which enable the user to:

* create *feedforward* neural networks of any size (also takes care of bias neurons)
* export and import the trained models
* load and vectorize images

As for the aforementioned neural network, it has a two-layers structure (strangely enough it shows the best result without any inner layers):

* *81* input neurons (9 * 9 pixels) + *1* bias neuron
* *10* output neurons (possible answer is a digit from 0 to 9)

The neural network is trained on a data set of 3000 images (each image is vectorized into the array of zeros and ones).
It requires around 30 cycles (images are shuffled for each cycle) of teaching via backpropagation method.
The model is then saved into a `model_save.csv` file for testing and could be loaded to any time.
The later test on a sample of 300 images usually indicates 75-85 % efficiency.
